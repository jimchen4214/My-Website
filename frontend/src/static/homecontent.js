const htmlContent = {
  html: `
  <br/>
  <h2><strong>Freedom, Privacy, Control: <br/>Embracing Simple, Open-Source Solutions</strong>

  </h2>
  <br/>


<h3 id="wiki"><strong>Wiki</strong></h3>
<ul>
  <li><strong>ArchLinux Wiki</strong>: <a href="https://wiki.archlinux.org/">wiki.archlinux.org</a></li>
  <li><strong>Gentoo Wiki</strong>: <a href="https://wiki.gentoo.org/">wiki.gentoo.org</a></li>
  <li><strong>Linux From Scratch</strong>: <a href="https://www.linuxfromscratch.org/lfs/read.html">linuxfromscratch.org/lfs/read.html</a></li>
  <li><strong>LineageOS Wiki</strong>: <a href="https://wiki.lineageos.org/">wiki.lineageos.org</a></li>
  <li><strong>PostmarketOS Wiki</strong>: <a href="https://wiki.postmarketos.org/">wiki.postmarketos.org</a></li>
    <li><strong>Asahi Linux</strong>: <a href="https://alx.sh/">alx.sh</a></li>

   <li><strong>Linux Kernel documentation</strong>: <a href="https://docs.kernel.org/">docs.kernel.org</a></li>
  <li><strong>FreeBSD Wiki</strong>: <a href="https://wiki.freebsd.org/">wiki.freebsd.org</a></li>      
  <li><strong>Cloudflare Docs</strong>: <a href="https://developers.cloudflare.com">developers.cloudflare.com</a></li>
  <li><strong>PostgreSQL</strong>: <a href="https://www.postgresql.org/docs/current/">postgresql.org/docs/current</a></li>
  <li><strong>MongoDB</strong>: <a href="https://www.mongodb.com/docs/">mongodb.com/docs</a></li>
  
 
</ul>


<h3 id="computer-networking"><strong>Networking</strong></h3>
<ul>
  <li><strong>VPN providers</strong>: <a href="https://guatizi.com/">Provider Lists</a>, <a href="https://github.com/hwanz/SSR-V2ray-Trojan-vpn">Cheap VPN</a> </li>
    <li><strong>Clash Verge</strong>: GUI for Clash <a href="https://github.com/clash-verge-rev/clash-verge-rev">Clash Verge Rev</a> </li>

  <li><strong>Cloudflare Warp</strong>: Cloudflare’s performance and security-focused VPN. <a href="https://1.1.1.1/">1.1.1.1</a></li>
  <li><strong>Tor Project</strong>: Browsing anonymously and dark web. <a href="https://www.torproject.org/">torproject.org</a></li>
  <li><strong>I2P</strong>: Invisible Internet Project: browsing the I2P
  anonymous network. <a href="https://geti2p.net/">geti2p.net</a></li>
</ul>


<h3 id="media"><strong>Self-Hosting</strong></h3>
<ul>
  <li><strong>Invidious</strong>: Access YouTube content privately through hosted instances. <a href="https://invidio.us/">invidio.us</a></li>
  <li><strong>PeerTube</strong>: Decentralized video hosting platform. <a href="https://github.com/Chocobozzz/PeerTube">PeerTube on GitHub</a></li>
  <li><strong>Mastodon</strong>: Decentralized social network. <a href="https://github.com/mastodon/mastodon">Mastodon on GitHub</a></li>
  <li><strong>Discourse</strong>: Open-source forum. <a href="https://github.com/discourse/discourse">Discourse on GitHub</a></li>
    <li><strong>Element</strong>: Secure Messaging. <a href="https://github.com/element-hq/element-web/">Element on GitHub</a></li>
      <li><strong>Libera Chat</strong>: Community Platform for FOSS projects. <a href="https://github.com/Libera-Chat/libera-chat.github.io">Libera Chat on GitHub</a></li>
</ul>


<h3 id="multipurpose-software"><strong>Multi-purpose software</strong></h3>
<ul>
  <li><strong>FFmpeg</strong>: Audio/Video processing. <a href="https://github.com/FFmpeg/FFmpeg">FFmpeg on GitHub</a></li>
  <li><strong>yt-dlp</strong>: Multiplatform and m3u8 video downloader. <a href="https://github.com/yt-dlp/yt-dlp">yt-dlp on GitHub</a> <br>Can be used in combination with <a href="https://github.com/puemos/hls-downloader">HLS Downloader on GitHub</a></li>
  <li><strong>pandoc</strong>: Universal document converter. <a href="https://github.com/jgm/pandoc">Pandoc on GitHub</a></li>
  <li><strong>selenium</strong>: Web Scraping. <a href="https://github.com/SeleniumHQ/selenium">selenium on GitHub</a></li>
  <li><strong>curl</strong>: Web Requests. <a href="https://github.com/curl/curl">Curl on Github</a></li>
</ul>

<h3 id="pricing"><strong>Pricing</strong></h3>
<ul>
  <li><strong>Free Resources for Developement</strong>: <a href="https://github.com/ripienaar/free-for-dev">github.com/ripienaar/free-for-dev</a></li>
  <li><strong>GPU Pricing</strong>: <a href="https://cloud-gpus.com/">cloud-gpus.com</a></li>
</ul>
    `,
};

export default htmlContent;
